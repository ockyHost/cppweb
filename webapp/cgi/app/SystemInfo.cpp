#include <webx/menu.h>

class SystemInfo : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(SystemInfo)

int SystemInfo::process()
{
	param_int(port);
	param_string(host);

	if (host.length() > 0)
	{
		SmartBuffer data = HttpRequest(getClassName()).getResult(host, port);

		if (data.isNull()) return simpleResponse(XG_ERROR);

		out << data.str();

		return XG_OK;
	}

	int res = XG_ERROR;
	long long free = 0;
	long long avail = 0;
	long long total = 0;

	if (GetDiskSpace(NULL, &total, &avail, &free))
	{
		json["diskfree"] = stdx::str(avail);
		json["disktotal"] = stdx::str(total);

		if (GetMemoryInfo(&total, &avail))
		{
			res = XG_OK;

			json["cpu"] = GetCpuCount();
			json["free"] = stdx::str(avail);
			json["total"] = stdx::str(total);
			json["percent"] = CalcCpuUseage(100);
		}
	}

	json["datetime"] = DateTime::ToString();
	json["code"] = res;
	out << json;

	return XG_OK;
}