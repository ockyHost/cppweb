#include <thread>
#include <string>
#include <iostream>
#include <clib/typedef.h>

#ifdef getch
#undef getch
#endif

#define getc __errinput__(__LINE__),__errexit__
#define gets __errinput__(__LINE__),__errexit__
#define getch __errinput__(__LINE__),__errexit__
#define scanf __errinput__(__LINE__),__errexit__
#define fgetc __errinput__(__LINE__),__errexit__
#define fgets __errinput__(__LINE__),__errexit__
#define fscanf __errinput__(__LINE__),__errexit__
#define getchar __errinput__(__LINE__),__errexit__

#ifdef cin
#undef cin
#endif
#define cin __errinput__(__LINE__),cin

#ifdef fstream
#undef fstream
#endif
#define fstream __errfilelog__(__LINE__);fstream

#ifdef ifstream
#undef ifstream
#endif
#define ifstream __errfilelog__(__LINE__);ifstream

#ifdef ofstream
#undef ofstream
#endif
#define ofstream __errfilelog__(__LINE__);ofstream

#ifdef fork
#undef fork
#endif
#define fork __errlog__(__LINE__),__errexit__

#ifdef kill
#undef kill
#endif
#define kill __errlog__(__LINE__),__errexit__

#ifdef pipe
#undef pipe
#endif
#define pipe __errlog__(__LINE__),__errexit__

#ifdef popen
#undef popen
#endif
#define popen __errlog__(__LINE__),__errexit__


#ifdef vfork
#undef vfork
#endif
#define vfork __errlog__(__LINE__),__errexit__

#ifdef signal
#undef signal
#endif
#define signal __errlog__(__LINE__),__errexit__

#ifdef mkfifo
#undef mkfifo
#endif
#define mkfifo __errlog__(__LINE__),__errexit__

#ifdef shmdt
#undef shmdt
#endif
#define shmdt __errlog__(__LINE__),__errexit__

#ifdef shmget
#undef shmget
#endif
#define shmget __errlog__(__LINE__),__errexit__

#ifdef semctl
#undef semctl
#endif
#define semctl __errlog__(__LINE__),__errexit__

#ifdef semget
#undef semget
#endif
#define semget __errlog__(__LINE__),__errexit__

#ifdef msgget
#undef msgget
#endif
#define msgget __errlog__(__LINE__),__errexit__

#ifdef msgctl
#undef msgctl
#endif
#define msgctl __errlog__(__LINE__),__errexit__

#ifdef execl
#undef execl
#endif
#define execl __errlog__(__LINE__),__errexit__

#ifdef execv
#undef execv
#endif
#define execv __errlog__(__LINE__),__errexit__

#ifdef execle
#undef execle
#endif
#define execle __errlog__(__LINE__),__errexit__

#ifdef execlp
#undef execlp
#endif
#define execlp __errlog__(__LINE__),__errexit__

#ifdef execvp
#undef execvp
#endif
#define execvp __errlog__(__LINE__),__errexit__

#ifdef execvpe
#undef execvpe
#endif
#define execvpe __errlog__(__LINE__),__errexit__

#ifdef link
#undef link
#endif
#define link __errlog__(__LINE__),__errexit__

#ifdef unlink
#undef unlink
#endif
#define unlink __errlog__(__LINE__),__errexit__

#ifdef symlink
#undef symlink
#endif
#define symlink __errlog__(__LINE__),__errexit__

#ifdef symlinkat
#undef symlinkat
#endif
#define symlinkat __errlog__(__LINE__),__errexit__

#ifdef open
#undef open
#endif
#define open __errlog__(__LINE__),__errexit__

#ifdef fopen
#undef fopen
#endif
#define fopen __errlog__(__LINE__),__errexit__

#ifdef creat
#undef creat
#endif
#define creat __errlog__(__LINE__),__errexit__

#ifdef mkdir
#undef mkdir
#endif
#define mkdir __errlog__(__LINE__),__errexit__

#ifdef rmdir
#undef rmdir
#endif
#define rmdir __errlog__(__LINE__),__errexit__

#ifdef remove
#undef remove
#endif
#define remove __errlog__(__LINE__),__errexit__

#ifdef rename
#undef rename
#endif
#define rename __errlog__(__LINE__),__errexit__

#ifdef system
#undef system
#endif
#define system __errlog__(__LINE__),__errexit__

#ifdef RunCommand
#undef RunCommand
#endif
#define RunCommand __errlog__(__LINE__),__errexit__

#ifdef CreateFolder
#undef CreateFolder
#endif
#define CreateFolder __errlog__(__LINE__),__errexit__

#ifdef CreateNewFile
#undef CreateNewFile
#endif
#define CreateNewFile __errlog__(__LINE__),__errexit__

static int __errexit__(...)
{
	exit(XG_DATAERR);
}

static int __errlog__(int line)
{
	fprintf(stderr, "第%d行：存在非法的系统调用(如果是自定义函数请重新命名)\n", line);

	fflush(stderr);

	exit(0);
}

static int __errinput__(int line)
{
	fprintf(stderr, "第%d行：暂不支持终端输入函数(您可以使用命令行启动参数)\n", line);

	fflush(stderr);

	exit(0);
}

static int __errfilelog__(int line)
{
	fprintf(stderr, "第%d行：暂不支持文件相关操作(fstream/ifstream/ofstream)", line);

	fflush(stderr);

	exit(0);
}

class __TimeoutKiller__
{
protected:
	time_t timeout;

public:
	__TimeoutKiller__()
	{
#ifdef XG_LINUX
		start(10);
#else
		start(15);
#endif
	}

public:
	void run()
	{
		while (timeout-- > 0) Sleep(999);

		exit(XG_TIMEOUT);
	}
	void start(int timeout)
	{
		this->timeout = timeout;

		std::thread([&](){
			this->run();
		}).detach();
	}
};

static __TimeoutKiller__ __timeoutkiller__;

