-- compiler
------------------------------------------------------------------
CC = "g++ -std=c++11"

-- compile setting
------------------------------------------------------------------
FLAG = "-DXG_WEBX_CPPSHELL -I. -I$SOURCE_HOME/library -I$PRODUCT_HOME/inc -I$PRODUCT_HOME/lib/openssl/inc -I$SOURCE_HOME/webapp/cpp/lib -I$SOURCE_HOME/webapp/app/compile/pub"

-- library link setting
------------------------------------------------------------------
LIB_LINK = "-L$PRODUCT_HOME/lib -lwebx -lhttp -ldbx.base -lopenssl -lzlib -ljson -lstdx -lclib -L$PRODUCT_HOME/lib/openssl/lib -lssl -lcrypto"