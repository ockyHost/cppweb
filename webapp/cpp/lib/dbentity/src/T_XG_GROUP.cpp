#include "T_XG_GROUP.h"


void CT_XG_GROUP::clear()
{
	this->id.clear();
	this->name.clear();
	this->icon.clear();
	this->menulist.clear();
	this->enabled.clear();
	this->remark.clear();
	this->statetime.clear();
}
int CT_XG_GROUP::insert()
{
	vector<DBData*> vec;

	sql = "INSERT INTO T_XG_GROUP(" + string(GetColumnString()) + ") VALUES(";
	sql += this->id.toValueString(conn->getSystemName());
	vec.push_back(&this->id);
	sql += ",";
	sql += this->name.toValueString(conn->getSystemName());
	vec.push_back(&this->name);
	sql += ",";
	sql += this->icon.toValueString(conn->getSystemName());
	vec.push_back(&this->icon);
	sql += ",";
	sql += this->menulist.toValueString(conn->getSystemName());
	vec.push_back(&this->menulist);
	sql += ",";
	sql += this->enabled.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->remark.toValueString(conn->getSystemName());
	vec.push_back(&this->remark);
	sql += ",";
	sql += this->statetime.toValueString(conn->getSystemName());
	sql += ")";

	return conn->execute(sql, vec);
}
bool CT_XG_GROUP::next()
{
	if (!rs) return false;

	sp<RowData> row = rs->next();

	if (!row) return false;

	this->id = row->getString(0);
	this->id.setNullFlag(row->isNull());
	this->name = row->getString(1);
	this->name.setNullFlag(row->isNull());
	this->icon = row->getString(2);
	this->icon.setNullFlag(row->isNull());
	this->menulist = row->getString(3);
	this->menulist.setNullFlag(row->isNull());
	this->enabled = row->getLong(4);
	this->enabled.setNullFlag(row->isNull());
	this->remark = row->getString(5);
	this->remark.setNullFlag(row->isNull());
	this->statetime = row->getDateTime(6);
	this->statetime.setNullFlag(row->isNull());

	return true;
}
sp<QueryResult> CT_XG_GROUP::find(const string& condition, const vector<DBData*>& vec)
{
	sql = "SELECT " + string(GetColumnString()) + " FROM T_XG_GROUP";

	if (condition.empty()) return rs = conn->query(sql);

	sql += " WHERE ";
	sql += condition;

	return rs = conn->query(sql, vec);
}
sp<QueryResult> CT_XG_GROUP::find()
{
	vector<DBData*> vec;
	vec.push_back(&this->id);
	return find(getPKCondition(), vec);
}
string CT_XG_GROUP::getPKCondition()
{
	string condition;
	condition = "ID=";
	condition += this->id.toValueString(conn->getSystemName());

	return stdx::replace(condition, "=NULL", "IS NULL");
}
int CT_XG_GROUP::update(bool nullable)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_GROUP SET ";
	if (nullable || !this->name.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "NAME=";
		sql += this->name.toValueString(conn->getSystemName());
		v.push_back(&this->name);
	}
	if (nullable || !this->icon.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ICON=";
		sql += this->icon.toValueString(conn->getSystemName());
		v.push_back(&this->icon);
	}
	if (nullable || !this->menulist.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "MENULIST=";
		sql += this->menulist.toValueString(conn->getSystemName());
		v.push_back(&this->menulist);
	}
	if (nullable || !this->enabled.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ENABLED=";
		sql += this->enabled.toValueString(conn->getSystemName());
	}
	if (nullable || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (nullable || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	v.push_back(&this->id);

	sql += " WHERE " + getPKCondition();

	return conn->execute(sql, v);
}
int CT_XG_GROUP::remove(const string& condition, const vector<DBData*>& vec)
{
	sql = "DELETE FROM T_XG_GROUP";

	if (condition.empty()) return conn->execute(sql);

	sql += " WHERE ";
	sql += condition;

	return conn->execute(sql, vec);
}
int CT_XG_GROUP::remove()
{
	vector<DBData*> vec;
	vec.push_back(&this->id);
	return remove(getPKCondition(), vec);
}
string CT_XG_GROUP::getValue(const string& key)
{
	if (key == "ID") return this->id.toString();
	if (key == "NAME") return this->name.toString();
	if (key == "ICON") return this->icon.toString();
	if (key == "MENULIST") return this->menulist.toString();
	if (key == "ENABLED") return this->enabled.toString();
	if (key == "REMARK") return this->remark.toString();
	if (key == "STATETIME") return this->statetime.toString();

	return stdx::EmptyString();
}
bool CT_XG_GROUP::setValue(const string& key, const string& val)
{
	if (key == "ID")
	{
		this->id = val;
		return true;
	}
	if (key == "NAME")
	{
		this->name = val;
		return true;
	}
	if (key == "ICON")
	{
		this->icon = val;
		return true;
	}
	if (key == "MENULIST")
	{
		this->menulist = val;
		return true;
	}
	if (key == "ENABLED")
	{
		this->enabled = val;
		return true;
	}
	if (key == "REMARK")
	{
		this->remark = val;
		return true;
	}
	if (key == "STATETIME")
	{
		this->statetime = val;
		return true;
	}

	return false;
}
int CT_XG_GROUP::update(const string& condition, bool nullable, const vector<DBData*>& vec)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_GROUP SET ";
	if (nullable || !this->name.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "NAME=";
		sql += this->name.toValueString(conn->getSystemName());
		v.push_back(&this->name);
	}
	if (nullable || !this->icon.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ICON=";
		sql += this->icon.toValueString(conn->getSystemName());
		v.push_back(&this->icon);
	}
	if (nullable || !this->menulist.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "MENULIST=";
		sql += this->menulist.toValueString(conn->getSystemName());
		v.push_back(&this->menulist);
	}
	if (nullable || !this->enabled.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ENABLED=";
		sql += this->enabled.toValueString(conn->getSystemName());
	}
	if (nullable || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (nullable || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	if (condition.empty()) return conn->execute(sql, v);

	sql += " WHERE " + condition;

	for (auto& item : vec) v.push_back(item);

	return conn->execute(sql, v);
}
