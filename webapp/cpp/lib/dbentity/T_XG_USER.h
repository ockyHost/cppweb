#ifndef XG_ENTITY_CT_XG_USER_H
#define XG_ENTITY_CT_XG_USER_H
///////////////////////////////////////////////////////////////////////
#include <dbx/DBConnect.h>


class CT_XG_USER : public DBEntity
{
public:
	bool next();
	void clear();
	int insert();
	int remove();
	sp<QueryResult> find();
	string getPKCondition();
	int update(bool nullable = false);
	string getValue(const string& key);
	bool setValue(const string& key, const string& val);
	int remove(const string& condition, const vector<DBData*>& vec = {});
	sp<QueryResult> find(const string& condition, const vector<DBData*>& vec = {});
	int update(const string& condition, bool nullable = false, const vector<DBData*>& vec = {});

	template<class DATA_TYPE, class ...ARGS>
	int remove(const string& condition, const DATA_TYPE& val, ARGS ...args)
	{
		vector<sp<DBData>> vec;

		DBConnect::Pack(vec, val, args...);

		vector<DBData*> tmp;

		for (auto& item : vec) tmp.push_back(item.get());

		return remove(condition, tmp);
	}
	template<class DATA_TYPE, class ...ARGS>
	sp<QueryResult> find(const string& condition, const DATA_TYPE& val, ARGS ...args)
	{
		vector<sp<DBData>> vec;

		DBConnect::Pack(vec, val, args...);

		vector<DBData*> tmp;

		for (auto& item : vec) tmp.push_back(item.get());
	
		sp<QueryResult> res = find(condition, tmp);

		if (res)
		{
			for (auto& item : vec) res->hold(item);
		}

		return res;
	}

public:
	DBString	user;
	DBString	dbid;
	DBString	name;
	DBString	passwd;
	DBString	menulist;
	DBString	grouplist;
	DBString	language;
	DBInteger	level;
	DBInteger	enabled;
	DBString	certno;
	DBString	mail;
	DBString	phone;
	DBString	address;
	DBString	remark;
	DBDateTime	statetime;

	static const char* GetTableName()
	{
		return "T_XG_USER";
	}

	static const char* GetColumnString()
	{
		return "USER,DBID,NAME,PASSWD,MENULIST,GROUPLIST,LANGUAGE,LEVEL,ENABLED,CERTNO,MAIL,PHONE,ADDRESS,REMARK,STATETIME";
	}
};

///////////////////////////////////////////////////////////////////////
#endif
