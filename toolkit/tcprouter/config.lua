-- 监听端口
PORT = 5555

-- 主机地址
HOST = "0.0.0.0"

-- 最否为SSL连接转发
SSL_ENABLED = false

-- PEM证书文件
CERT_FILEPATH = "cert.crt"

-- PEM私钥文件
PRIKEY_FILEPATH = "cert.key"

-- 目的地址列表(地址:端口:权重|地址:端口:权重)
DEST_HOST_LIST = "127.0.0.1:8888"

-- 地址白名单(地址|地址)
WHITE_HOST_LIST = ""

-- 地址黑名单(地址|地址)
BLACK_HOST_LIST = ""

-- 同一地址每分钟连接上限
ADDRESS_CONNECT_PERMIN = 1000

-- 日志存放目录
LOGFILE_PATH = "log"

-- 单个日志文件最大大小(KB)
LOGFILE_MAXSIZE = 10000
